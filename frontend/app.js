var imageParent = document.getElementById('game');
/**
 * Clear previous output before showing the current output
 */

var clearSymbols = function () {
    imageParent.innerHTML = "";
};

/**
 * Dynamically changing the win type based on random number
 */

var displayWinType = function (numbers) {
    var winType = 'No Win';
    if (numbers[0] === numbers[1]
        && numbers[1] === numbers[2]
        && numbers[0] === numbers[2]) {
        winType = 'Big Win';
    } else if (numbers[0] === numbers[1]
        || numbers[1] === numbers[2]
        || numbers[0] === numbers[2]) {
        winType = 'Small Win';
    }
    document.getElementById('winType').textContent = winType;
};

/**
 * Following function will display the new output on the UI 
*/

var refreshSymbols = function (response, next) {
    var numbers = response.numbers;
    var images = [
        'Symbol_0.png',
        'Symbol_1.png',
        'Symbol_2.png',
        'Symbol_3.png',
        'Symbol_4.png',
        'Symbol_5.png',
        'Symbol_6.png'
    ];

    var i = 0;

    clearSymbols();
    displayWinType(numbers);

    while (i < 3) {
        var x = document.createElement("IMG");
        x.setAttribute("src", '/assets/' + images[numbers[i]]);
        x.setAttribute("width", "100");
        x.setAttribute("height", "100");
        imageParent.appendChild(x);
        i++;
    };

    //Check if bonus should be trigger or not.
    if (response.getBonus) {
        setTimeout(function () {
            this.refresh();
        }, 1000);
    }
};

/**
 * This function will trigger on refresh button click and will fetch 
 * random number from the server.
 */
var refresh = function () {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            refreshSymbols(JSON.parse(this.responseText));
        }
    };
    xhttp.open("GET", "/refresh", true);
    xhttp.send();
};
