const express = require('express');
const app = express();
const path = require('path');

const generateRandomNumber = (req, res) => {
    let numbers = [];
    for (let i = 0; i < 3; i++) {
        let temp = Math.round((Math.random() * 5));
        numbers.push(temp);
    };

    let responseData = {
        numbers: numbers,
        getBonus: Math.round((Math.random()))
    };
    
    res.status(200).send(responseData);
};

//Serve static content
app.use(express.static(__dirname + '/frontend'));

//Handle uncaught exception
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

app.get('/refresh', generateRandomNumber);
app.listen(3000);